import React from "react";
import { Link } from "react-router-dom";
import { Card, Button, Col, Stack} from "react-bootstrap";
import { useCart } from "react-use-cart";

import { useState, useEffect } from "react"

const ProductCard = (props) => {

	const { addItem } = useCart()

	// console.log(props.productProp._id)

	const [myProduct, setMyProduct] = useState()


	useEffect(() => {
	setMyProduct({
	  id: props.productProp._id,
	  imageURL: props.productProp.imageURL,
	  name: props.title,
	  description: props.description,
	  price: props.price
	});
	}, []);

	// console.log(myProduct)

	return(
		
		<Col xs={12} md={3}>
		    <Card border="primary" className="cardHighlight">
		    	<Card.Img variant="top" src={`https://drive.google.com/uc?export=view&id=${props.productProp.imageURL}`} className="img-fluid my-image"/>
		        <Card.Body>
		           	<Card.Header>{props.name}</Card.Header>
		            <Card.Text style={{fontSize:"16px"}}>
		               {props.description}
		            </Card.Text>  
		        </Card.Body>
		        
		        <Card.Footer className="text-center p-3">
		        	<div className="container">
		                <Card.Text className="fw-bold text-start">
		    	               Price: &#8369;{props.price}<br/>
		    	               Stocks: {props.stocks}
		    	        </Card.Text>
		        	</div>

		        	<Button className="btn btn-success" onClick={() => addItem(myProduct)}>Add to Cart</Button>
		        	<Button as={Link} to={`/products/${props._id}`} size="sm" className="margin-left" variant="danger">Buy Now</Button>
		        </Card.Footer>
		    </Card>
		</Col>

		




	)
}

export default ProductCard



// import { useState, useEffect } from "react";
// import { Link } from "react-router-dom";
// import { Card, Button, Col } from "react-bootstrap";

// //Deconstruct the "productProp" form the "props" object to shorten syntax.
// export default function ProductCard({productProp}){

	

// 	// Deconstruct productProp properties into their own variable
// 	const {_id, name, description, price, imageURL, stocks } = productProp;	

	
// return(
// 	<Col xs={12} md={3}>
// 		    <Card border="primary" className="cardHighlight">
// 		    	<Card.Img variant="top" src={`https://drive.google.com/uc?export=view&id=${imageURL}`} className="img-fluid"/>
// 		    	<Card.Header>{name}</Card.Header>
// 		        <Card.Body>
// 		            <Card.Title>
// 		                Description:
// 		            </Card.Title>
// 		            <Card.Text>
// 		               {description}
// 		            </Card.Text>
// 		            <Card.Title>
// 		                Price:
// 		            </Card.Title>
// 		            <Card.Text>
// 		               {price}
// 		            </Card.Text>
// 		            <Card.Text>
// 		               stocks: {stocks}
// 		            </Card.Text>
// 		        </Card.Body>
// 		        <Card.Footer>
// 		        	<Button as={Link} to={`/products/${_id}`}>Details</Button>
// 		        </Card.Footer>
// 		    </Card>
// 		</Col>
// 	)
// }
