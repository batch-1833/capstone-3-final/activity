import {Navbar, Container} from "react-bootstrap";

export default function Footer(){

	return(
		<Navbar bg="dark" className="container-fluid">
		<Navbar variant="dark" className="p-3">
		          <Navbar.Brand>
		            <p>&copy; Copyright <strong>William Louis L. Alambatin</strong>. All Rights Reserved 2022</p>
		          </Navbar.Brand>
		      </Navbar>
		</Navbar>


	)
}