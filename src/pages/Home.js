import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home(){
	const data = {
		title: "Digital Shop Online",
		content: "Your One Stop Digital Shop.",
		destination: "/product",
		label: "Order Now"
	}

	return(
		<>
			<Banner data={data}/>
        	{<Highlights />}
		</>
	)
}